<h1 align="center">
  <a href="https://poketube.fun/watch?v=9sJUDx7iEJw&quality=medium&=sjohgteojgytrueugtye4jhtytjrjnyıı">
   <img src="https://poketube.fun/css/logo.svg" width="400"> 
   </a>
</h1>

<div align="center">

[Welcome!](#welcome)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;[No None-free codec needed](#no-non-free-codec-needed-3)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;[Hosting Poketube~](#hosting-poketube)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;[PokeTube community!](#poketube-community)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;[The Legal Stuff (boring tbh)](#the-legal-stuff-boring-tbh)

<a href="https://tosdr.org/en/service/7114">
   <img src="https://shields.tosdr.org/en_7114.svg"/>
</a>
<img src="https://raw.githubusercontent.com/vshymanskyy/StandWithUkraine/main/badges/StandWithUkraine.svg">
<a href="./LICENSE"><img src="https://img.shields.io/badge/License-GPL--3-FF6666" alt="License - GPL-3"></a>

</div>

## Welcome!

This is the source code of PokeTube, the privacy-friendly youtube front-end built with the InnerTube API ([Docs](https://docs.poketube.fun)) that's packed with some pretty cool stuff including:

- ZERO ads
- Lyrics to songs
- A clean and modern UI
- A Javascript-free frontend
- No cookies or data collection
- And built-in dislike counts Thaks to the [Return YouTube Dislike Api](https://www.returnyoutubedislike.com/)!

## No Non-free codec needed :3

PokeTube uses openh264 which is free software! poketube does not inculude non free stuff owowowoow!!!!

you can view the source code of the openh264 codec in this repo :3 --> https://github.com/cisco/openh264.git

PLEASE NOTE THAT THIS SOFTWARE MAY INCULUDE CODECS THAT IN CERTAIN COUNTRIES MAY BE COVERED BY PATENTS OR HAVE LEGAL ISSUES. PATENT AND COPYRIGHT LAWS OPERATE DIFFERENTLY DEPENDING ON WHICH COUNTRY YOU ARE IN. PLEASE OBTAIN LEGAL ADVICE IF YOU ARE UNSURE WHETHER A PARTICULAR PATENT OR RESTRICTION APPLIES TO A CODEC YOU WISH TO USE IN YOUR COUNTRY.

## Hosting Poketube~

- To self host your own Poketube instance, you'll need the following:

  - [Node.js](https://nodejs.org/en/download/)
  - [npm](http://npmjs.com) (Included with Node.js)

Once you have everything, clone our repo:

```
git clone https://codeberg.org/ashley/poketube.git
```

You can also clone using our Github mirror if you'd prefer:

```
git clone https://github.com/ashley0143/poketube.git
```

Now, install the needed dependencies within the Poketube folder:

```
npm install 
```

Once everythings installed, start your server with the following command:

```
node server.js
```

Congrats, Poketube should now be running on `localhost:3000`!

## PokeTube community!

Join the community on [revolt](https://rvlt.gg/poketube) :3

## The Legal Stuff (boring tbh)

[Code Of conduct](https://codeberg.org/Ashley/poketube/src/branch/main/CODE_OF_CONDUCT.md)

[Privacy Policy](https://poketube.fun/privacy)

TL;DR: we dont collect or share your personal info, that's it lol.

We additionally use the GNU Coding Standard, see [this link.](https://www.gnu.org/prep/standards)
